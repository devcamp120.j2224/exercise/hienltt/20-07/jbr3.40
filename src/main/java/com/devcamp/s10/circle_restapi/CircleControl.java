package com.devcamp.s10.circle_restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleControl {
    @CrossOrigin
    @GetMapping("/circle-are")
    public double getCircleAre() {

        Circle cir1 = new Circle(2.0);
        System.out.println(cir1);

        return cir1.getArea();
    }
}
